import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { HttpModule } from '@angular/http'
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ListaPageModule } from '../pages/lista/lista.module';
import { CarritoProvider } from '../providers/carrito/carrito';

import { IonicStorageModule } from '@ionic/storage';
import { ApiRestProvider } from '../providers/api-rest/api-rest';
import { LoginPageModule } from '../pages/login/login.module';
import { StorageProvider } from '../providers/storage/storage';
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage
  ],
  imports: [
    HttpModule,
    BrowserModule,
    ListaPageModule,
    LoginPageModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    CarritoProvider,
    ApiRestProvider,
    StorageProvider
  ]
})
export class AppModule {}
