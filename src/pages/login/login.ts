import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoadingController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { ApiRestProvider } from '../../providers/api-rest/api-rest';
import { AlertController } from 'ionic-angular';
import { CarritoProvider } from '../../providers/carrito/carrito';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  formularioUsuario:FormGroup;
  Correo:any
  Contrasena:any
  constructor( public alertCtrl: AlertController,private api:ApiRestProvider,
     public navCtrl: NavController, public navParams: NavParams,
      public fb: FormBuilder,public loadingCtrl: LoadingController,
      public storage:CarritoProvider) {
    this.buildForm();
  }

  buildForm() {
      
    this.formularioUsuario = this.fb.group({
      correo:['',[Validators.required,Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)]],
      password:['',[Validators.required,]],
      
    });
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  gologin(){

    let loading = this.loadingCtrl.create({
      content: "Espere por favor..."
    });

    loading.present();

    let data = {
      "username": this.Correo,
      "password": this.Contrasena
     } 

     this.api.login(data).subscribe(apiData=>{
       console.log(apiData)
       
      let token = this.storage.ajustes.token
      console.log(token)
       loading.dismiss();
       /*this.navCtrl.setRoot(HomePage)*/
     },erro=>{
      loading.dismiss();
      const alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: 'Contraseña Incorrecta',
        buttons: ['OK']
      });
      alert.present();


     })

    

     console.log(data)
     
     
         

  }


}
