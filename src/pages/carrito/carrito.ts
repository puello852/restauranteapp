import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Item, ToastController } from 'ionic-angular';
import { CarritoProvider } from '../../providers/carrito/carrito';
import { AlertController } from 'ionic-angular';
import { ApiRestProvider } from '../../providers/api-rest/api-rest';
import { URLSearchParams } from '@angular/http';
/**
 * Generated class for the CarritoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-carrito',
  templateUrl: 'carrito.html',
})
export class CarritoPage {
  [x: string]: any;
  total=0
  pedido:boolean=false
  espera:boolean=true
  array_producto=[]
  vamospapu: any;
  constructor(public alertCtrl: AlertController,public toastCtrl: ToastController,public navCtrl: NavController, public navParams: NavParams, 
    public viewCtrl: ViewController, private carritos: CarritoProvider,private api:ApiRestProvider) {
  }

  ionViewDidLoad() {
    for (let item of this.carritos.item){
     
       this.total= this.total + item.precio
      }
      console.log(this.total)
   
  }


  borrar(item){
     let pos = 0
    for (let items of this.carritos.item){
      console.log(items.id_plato)
      if(item.id_plato == items.id_plato){
        this.carritos.item.splice(pos,1)
        const toast = this.toastCtrl.create({
          message: 'Elimando Exitosamente',
          duration: 3000
        });
        toast.present(); 
      }
      pos++
    }
    console.log(this.carritos.item)
  }

  enviar(){
    
    for(let item of this.carritos.item){
      this.array_producto.push({id_plato:item.id_plato,cantidad:1})
    }
    
    this.vamospapu ={"array_producto":this.array_producto}
    console.log(this.vamospapu)
    this.array_producto=[]
    

    const confirm = this.alertCtrl.create({
      title: '¿Estas seguro?',
      message: 'Al aceptar enviaras tu pedido a la caja',
      buttons: [
        {
          text: 'Ahora no',
          handler: () => {
          
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
           
            let urlSearchParams = new URLSearchParams();
            urlSearchParams.append('nombre_cliente', "Edwin");
            urlSearchParams.append('id_mesa', "1");
            console.log(urlSearchParams)
            this.api.EnviarPedido(urlSearchParams, this.vamospapu).subscribe(apiData=>{
              console.log(apiData)
              this.idpedido=apiData.id_pedido
              this.espera=false
              this.pedido=true
            })
          }
        }
      ]
    });
    confirm.present()
  }

  cancelar(){
    const confirm = this.alertCtrl.create({
     title: '¿Estas seguro?',
      message: 'Al aceptar Cancelaras tu pedido a la caja',
      buttons: [
        {
          text: 'Ahora no',
          handler: () => {
          
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            let data = {
              id_pedido:this.idpedido
            }
            this.api.CancelarPedidos(data).subscribe(apiData=>{
              this.espera=true
              this.pedido=false
            })
          }
        }
      ]
    });
    confirm.present()
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
